package nlmk.nikulinvs.tm.controller;

import nlmk.nikulinvs.tm.entity.Project;
import nlmk.nikulinvs.tm.enumerated.Role;
import nlmk.nikulinvs.tm.service.ProjectService;
import nlmk.nikulinvs.tm.service.UserService;


import java.util.List;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;

    private final UserService userService;

    public ProjectController(ProjectService projectService, UserService userService) {
        this.projectService = projectService;
        this.userService = userService;
    }

    public int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectService.create(name, description, userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public int updateProjectById() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER, PROJECT id:");
        final long id = scanner.nextLong();
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER, PROJECT INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int updateProjectByName() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER, PROJECT NAME:");
        final String name = scanner.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String new_name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), new_name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long id = scanner.nextLong();
        final Project project = projectService.removeById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("PLEASE, ENTER PROJECT INDEX:");
        final int index = scanner.nextInt() - 1;
        final Project project = projectService.removeByIndex(index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        final Project project = projectService.removeByName(name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("USER_ID: " + project.getUserId());
        System.out.println("[OK]");
    }

    public int viewProjectById() {
        System.out.println("ENTER, PROJECT ID:");
        final long id = scanner.nextLong();
        final Project project = projectService.findById(id);
        viewProject(project);
        return 0;
    }

    public int viewProjectByIndex() {
        System.out.println("ENTER, PROJECT INDEX:");
        final int index = scanner.nextInt() - 1;
        final Project project = projectService.findByIndex(index);
        viewProject(project);
        return 0;
    }

    public int viewProjectByName() {
        System.out.println("ENTER, PROJECT NAME:");
        final String name = scanner.nextLine();
        final Project project = projectService.findByName(name);
        viewProject(project);
        return 0;
    }

    public int listProject() {
        System.out.println("[LIST PROJECT]");
        int index = 1;
        List<Project> projectList;
        if (userService.currentUser == null) {
            System.out.println("[FAIL]");
        } else {
            if (userService.currentUser.getDisplayName().equals(Role.ADMIN.getDisplayName()))
                projectList = projectService.findAll();
            else
                projectList = projectService.findAllByUserId(userService.currentUser.getId());
            for (final Project project : projectList) {
                System.out.println(index + ". " + project.getId() + ": " + project.getName());
                index++;
            }
            if (index > 1) System.out.println("[OK]");
            else System.out.println("[PROJECTS ARE NOT FOUND]");
        }

        return 0;

    }

}
