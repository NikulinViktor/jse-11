package nlmk.nikulinvs.tm;


import nlmk.nikulinvs.tm.controller.ProjectController;
import nlmk.nikulinvs.tm.controller.TaskController;
import nlmk.nikulinvs.tm.controller.UserController;
import nlmk.nikulinvs.tm.controller.SystemController;
import nlmk.nikulinvs.tm.enumerated.Role;
import nlmk.nikulinvs.tm.repository.ProjectRepository;
import nlmk.nikulinvs.tm.repository.TaskRepository;
import nlmk.nikulinvs.tm.repository.UserRepository;
import nlmk.nikulinvs.tm.service.ProjectService;
import nlmk.nikulinvs.tm.service.ProjectTaskService;
import nlmk.nikulinvs.tm.service.TaskService;
import nlmk.nikulinvs.tm.service.UserService;
import static nlmk.nikulinvs.tm.constant.TerminalConst.*;

import java.util.Arrays;
import java.util.Scanner;



public class App {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final UserRepository userRepository = new UserRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final UserService userService = new UserService(userRepository, projectRepository, taskRepository);

    private final ProjectController projectController = new ProjectController(projectService, userService);

    private final TaskController taskController = new TaskController(taskService, projectTaskService, userService);

    private final UserController userController = new UserController(userService);

    private final SystemController systermController = new SystemController();

    {
        projectRepository.create("PROJECT_A", "DESCRIPTION_PROJECT_ADMIN");
        projectRepository.create("PROJECT_T", "DESCRIPTION_PROJECT_TEST");
        taskRepository.create("TASK_A", "DESCRIPTION_TASK_ADMIN");
        taskRepository.create("TASK_T", "DESCRIPTION_TASK_TEST");
        userRepository.create("nikulin_vs", "Viktor", "  ", "NIkulin", "11111", Role.ADMIN);
        userRepository.create("r2d2", "robot", "droid", "starwars", "00000", Role.USER);
    }

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final App app = new App();
        app.run(args);
        app.systermController.displayWelcome();
        String command = "";
        while (!CMD_EXIT.equals(command)) {
            command = scanner.nextLine();
            app.run(command);
        }
    }

    public void run(final String[] args) {
        if (args == null) {
            return;
        }
        if (args.length < 1) {
            return;
        }
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    public int run(final String param) {
        if (param == null || param.isEmpty()) {
            return -1;
        }
        if (!Arrays.asList(ALL_ACTIONS).contains(param)) {
            if (userController.checkAuthorisation(param) == -1) {
                return -1;
            }
        }
        switch (param) {
            case LOG_ON: return userController.registry();
            case LOG_OFF: return userController.logOff();
            case USER_INFO: return userController.displayUserInfo();
            case USER_UPDATE: return userController.updateUser();
            case USER_UPDATE_PASSWORD: return userController.updatePassword();

            case CMD_VERSION: return systermController.displayVersion();
            case CMD_ABOUT: return systermController.displayAbout();
            case CMD_HELP: return systermController.displayHelp();
            case CMD_EXIT: return systermController.displayExit();

            case CMD_PROJECT_CREATE: return projectController.createProject();
            case CMD_PROJECT_CLEAR: return projectController.clearProject();
            case CMD_PROJECT_LIST: return projectController.listProject();
            case PROJECT_VIEW_BY_ID: return projectController.viewProjectById();
            case PROJECT_VIEW_BY_INDEX: return projectController.viewProjectByIndex();
            case PROJECT_VIEW_BY_NAME: return projectController.viewProjectByName();
            case CMD_PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
            case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_UPDATE_BY_ID: return projectController.updateProjectById();
            case CMD_PROJECT_REMOVE_BY_INDEX: return projectController.updateProjectByIndex();
            case PROJECT_UPDATE_BY_NAME: return projectController.updateProjectByName();

            case CMD_TASK_CREATE: return taskController.createTask();
            case CMD_TASK_CLEAR: return taskController.clearTask();
            case CMD_TASK_LIST: return taskController.listTask();
            case TASK_VIEW_BY_ID: return taskController.viewTaskById();
            case TASK_VIEW_BY_INDEX: return taskController.viewTaskByIndex();
            case TASK_VIEW_BY_NAME: return taskController.viewTaskByName();
            case CMD_TASK_REMOVE_BY_ID: return taskController.removeTaskById();
            case CMD_TASK_REMOVE_BY_INDEX: return taskController.removeTaskByIndex();
            case CMD_TASK_REMOVE_BY_NAME: return taskController.removeTaskByName();
            case TASK_UPDATE_BY_ID: return taskController.updateTaskById();
            case CMD_TASK_UPDATE_BY_INDEX: return taskController.updateTaskByIndex();
            case TASK_UPDATE_BY_NAME: return taskController.updateTaskByName();
            case CMD_TASK_ADD_TO_PROJECT_BY_IDS: return taskController.addTaskToProjectByIds();
            case CMD_TASK_REMOVE_FROM_PROJECT_BY_IDS: return taskController.removeTaskToProjectByIds();
            case CMD_TASK_LIST_BY_PROJECT_ID: return taskController.listTaskByProjectId();

            case USER_CREATE: return userController.createUser();
            case USER_CLEAR: return userController.clearUser();
            case USER_LIST: return userController.listUser();
            case USER_VIEW_BY_ID: return userController.viewUserById();
            case USER_VIEW_BY_INDEX: return userController.viewUserByIndex();
            case USER_VIEW_BY_LOGIN: return userController.viewUserByLogin();
            case USER_REMOVE_BY_ID: return userController.removeUserById();
            case USER_REMOVE_BY_INDEX: return userController.removeUserByIndex();
            case USER_REMOVE_BY_LOGIN: return userController.removeUserByLogin();
            case USER_UPDATE_BY_ID: return userController.updateUserById();
            case USER_UPDATE_BY_INDEX: return userController.updateUserByIndex();
            case USER_UPDATE_BY_LOGIN: return userController.updateUserByLogin();
            case USER_ASSIGN_PROJECT: return userController.addUsertoProjectbyIds();
            case USER_REMOVE_PROJECT: return userController.removeUsertoProjectbyIds();
            case USER_ASSIGN_TASK: return userController.addUsertoTaskbyIds();
            default: return systermController.displayError();
        }

    }

}
